package com.techieteamltd.vajrotask.UIComp

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.techieteamltd.vajrotask.APIComp.Config.APIConfig
import com.techieteamltd.vajrotask.APIComp.Config.ServiceEndPoint
import com.techieteamltd.vajrotask.APIComp.Pojos.ProductListResponse
import com.techieteamltd.vajrotask.Adapters.ProductListAdapter
import com.techieteamltd.vajrotask.R
import com.techieteamltd.vajrotask.ViewModel.CartViewModel
import com.techieteamltd.vajrotask.ViewModel.ProductViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class HomeScreenActivity : AppCompatActivity() {

    private lateinit var productViewModel: ProductViewModel

    private lateinit var cartViewModel: CartViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_home_screen)
        if (savedInstanceState == null) {

        val toolbar = findViewById(R.id.toolbar) as Toolbar?

        val gotoCarts = toolbar!!.findViewById(R.id.gotoCarts) as ImageButton?


        setSupportActionBar(toolbar)


        gotoCarts!!.setOnClickListener(View.OnClickListener {

            var gotoCartIntent = Intent(this@HomeScreenActivity,CartScreenActivity::class.java)
            startActivity(gotoCartIntent)

        })


        getProductList()

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview)

        val adapter = ProductListAdapter(this)

        recyclerView.adapter = adapter

        recyclerView.layoutManager = LinearLayoutManager(this)

        productViewModel = ViewModelProvider(this).get(ProductViewModel::class.java)

        cartViewModel = ViewModelProvider(this).get(CartViewModel::class.java)

        productViewModel.allProducts.observe(this, Observer { products ->
            products?.let {

                adapter.setProduct(it)

            }
        })

        }


    }

    fun getProductList(){

        val request = APIConfig.buildService(ServiceEndPoint::class.java)

        val call = request.getProducts()

        call.enqueue(object : Callback<ProductListResponse> {

            override fun onResponse(call: Call<ProductListResponse>, response: Response<ProductListResponse>) {

                if (response.isSuccessful){

                    productViewModel.insertAll(response.body()!!.products)
                }

            }
            override fun onFailure(call: Call<ProductListResponse>, t: Throwable) {

                Toast.makeText(this@HomeScreenActivity, "${t.message}", Toast.LENGTH_SHORT).show()

            }
        })
    }

    override fun onBackPressed() {

        finish()
    }
}

