package com.techieteamltd.vajrotask.UIComp

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageButton
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.techieteamltd.vajrotask.Adapters.CartListAdapter
import com.techieteamltd.vajrotask.R
import com.techieteamltd.vajrotask.Room.Entity.CartRoom
import com.techieteamltd.vajrotask.ViewModel.CartViewModel
import com.techieteamltd.vajrotask.ViewModel.ProductViewModel
import java.text.NumberFormat
import java.util.*


class CartScreenActivity : AppCompatActivity() {

    private lateinit var cartViewModel: CartViewModel

    lateinit var pids : List<String>

    lateinit var priceArray : List<String>

    private lateinit var productViewModel: ProductViewModel

    lateinit var totalCountText :TextView
    lateinit var noProductAvail :TextView
    lateinit var totalSec :RelativeLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_cart_screen)
        val toolbar = findViewById(R.id.toolbar) as Toolbar?

        val backBtn = toolbar!!.findViewById(R.id.backBtn) as ImageButton?

        setSupportActionBar(toolbar)


        backBtn!!.setOnClickListener(View.OnClickListener {

            finish()

        })
        cartViewModel = ViewModelProvider(this).get(CartViewModel::class.java)

        productViewModel = ViewModelProvider(this).get(ProductViewModel::class.java)

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview)

        totalCountText = findViewById<TextView>(R.id.totalCountText)

        noProductAvail = findViewById<TextView>(R.id.noProductAvail)

        totalSec = findViewById<RelativeLayout>(R.id.totalSec)

        val adapter = CartListAdapter(this)

        recyclerView.adapter = adapter

        recyclerView.layoutManager = LinearLayoutManager(this)

        cartViewModel.validcartList.observe(this, Observer { cartList ->
            cartList?.let {

                pids = getProductArray(cartList)

                productViewModel.searchByCartId(pids)

            }
        })

        cartViewModel.allCartList.observe(this, Observer { products ->

            products?.let {

                priceArray = getTotalAmount(products)

                val result = priceArray.map { it.toLong() }
                val indiaLocale = Locale("en", "IN")

                val format: NumberFormat = NumberFormat.getCurrencyInstance(indiaLocale)

                val currencyTotal: String = format.format(result.sum())
                if(result.sum().equals(0.toLong())) {
                    totalSec.visibility = View.GONE
                    noProductAvail.visibility = View.VISIBLE
                }else{
                    totalSec.visibility = View.VISIBLE
                    noProductAvail.visibility = View.GONE
                    totalCountText.text = "Total : " + currencyTotal
                }

                }
            })

        try {
            productViewModel.cartProductObservable!!.observe(this, Observer { pList ->

                pList?.let {

                    adapter.setCartProduct(it)

                }
            })

        }catch (e:Exception){

            e.printStackTrace()
        }


    }

    private fun getProductArray(list: List<CartRoom?>) = list.mapNotNull { it?.pid }.toList()
    private fun getTotalAmount(list: List<CartRoom?>) = list.mapNotNull { it?.price }.toList()



}
