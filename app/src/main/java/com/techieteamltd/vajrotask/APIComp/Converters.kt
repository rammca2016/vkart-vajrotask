package com.techieteamltd.vajrotask.APIComp

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*

class Converters {

    private val gson = Gson()
    @TypeConverter
    fun stringToList(data: String?): List<Any> {
        if (data == null) {
            return Collections.emptyList()
        }

        val listType = object : TypeToken<List<Any>>() {

        }.type

        return gson.fromJson<List<Any>>(data, listType)
    }

    @TypeConverter
    fun listToString(someObjects: List<Any>): String {
        return gson.toJson(someObjects)
    }
}

