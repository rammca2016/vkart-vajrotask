package com.techieteamltd.vajrotask.APIComp.Config

import com.techieteamltd.vajrotask.APIComp.Pojos.ProductListResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ServiceEndPoint {

    @GET("/v2/5def7b172f000063008e0aa2")
    fun getProducts(): Call<ProductListResponse>

}