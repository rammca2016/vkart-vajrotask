package com.techieteamltd.vajrotask.APIComp.Pojos

import com.techieteamltd.vajrotask.Room.Entity.ProductRoom

data class ProductListResponse(
    val products: List<ProductRoom>
)