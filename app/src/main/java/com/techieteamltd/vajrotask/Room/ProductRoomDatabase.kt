package com.techieteamltd.vajrotask.Room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import com.techieteamltd.vajrotask.APIComp.Converters
import com.techieteamltd.vajrotask.Room.Dao.CartDao
import com.techieteamltd.vajrotask.Room.Dao.ProductDao
import com.techieteamltd.vajrotask.Room.Entity.CartRoom
import com.techieteamltd.vajrotask.Room.Entity.ProductRoom

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@Database(entities = [ProductRoom::class, CartRoom::class], version = 1)
@TypeConverters(Converters::class)
abstract class ProductRoomDatabase  : RoomDatabase() {
    abstract fun productDao(): ProductDao
    abstract fun cartDao(): CartDao

    companion object {
        @Volatile
        private var INSTANCE: ProductRoomDatabase? = null

        fun getDatabase(
            context: Context,
            scope: CoroutineScope
        ): ProductRoomDatabase {

            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    ProductRoomDatabase::class.java,
                    "product_database"
                )
                    .fallbackToDestructiveMigration()
                    .addCallback(ProductDatabaseCallback(scope))
                    .build()
                INSTANCE = instance
                // return instance
                instance
            }
        }

        private class ProductDatabaseCallback(
            private val scope: CoroutineScope
        ) : RoomDatabase.Callback() {

            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)

                INSTANCE?.let { database ->
                    scope.launch(Dispatchers.IO) {
                        populateDatabase(database.productDao())
                        populateCartDatabase(database.cartDao())
                    }
                }
            }
        }


        fun populateDatabase(productDao: ProductDao) {
            productDao.deleteAll()
        }
        fun populateCartDatabase(cartDao: CartDao) {
//            cartDao.deleteAll()
        }
    }
}