package com.techieteamltd.vajrotask.Room.Dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.techieteamltd.vajrotask.Room.Entity.CartRoom

@Dao
interface CartDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(cartRoom: CartRoom)

    @Query("SELECT * from cart_table ORDER BY pid ASC")
    fun getAlphabetizedCart(): LiveData<List<CartRoom>>


    @Query("SELECT * from cart_table")
    fun getAll(): List<CartRoom>

    @Query("SELECT * from cart_table  WHERE qty != 0")
    fun getCartList(): LiveData<List<CartRoom>>

    @Query("SELECT pid FROM cart_table WHERE pid = :id LIMIT 1")
    fun getItemId(id: String): String?


    @Query("SELECT qty FROM cart_table WHERE pid = :id LIMIT 1")
    fun getItemQty(id: String): Long?


    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(cartRoom: CartRoom)

    @Query("DELETE FROM cart_table")
    fun deleteAll()

    @Query("DELETE FROM cart_table WHERE pid = :id")
    fun deleteByPid(id: String)
}