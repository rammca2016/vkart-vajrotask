package com.techieteamltd.vajrotask.Room.Entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "cart_table")
data class CartRoom (
    @PrimaryKey
    @ColumnInfo(name = "pid")
    val pid: String,
    @ColumnInfo(name = "price")
    val price: String,
    @ColumnInfo(name = "qty")
    val qty: Long?

)