package com.techieteamltd.vajrotask.Room.Repository

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.techieteamltd.vajrotask.Room.Dao.ProductDao
import com.techieteamltd.vajrotask.Room.Entity.ProductRoom


class ProductRepository(private val productDao: ProductDao) {

    val allProductList: LiveData<List<ProductRoom>> = productDao.getAlphabetizedProduct()
//    val allValidCartList: LiveData<List<ProductRoom>> =  MutableLiveData<ProductRoom>();



    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(product: ProductRoom) {
        productDao.insert(product)
    }


    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insertAll(productList: List<ProductRoom>) {
        productDao.deleteAll()
        productDao.insertAllProduct(productList)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
     fun getValidCartProduct(cartproductIds: List<String>) :LiveData<List<ProductRoom>> {

        var allValidCartList =
            productDao.getCartProduct(cartproductIds) as LiveData<List<ProductRoom>>
        return allValidCartList
    }
}
