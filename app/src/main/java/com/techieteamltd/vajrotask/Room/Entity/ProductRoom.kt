package com.techieteamltd.vajrotask.Room.Entity

import androidx.annotation.Nullable
import androidx.room.*

@Entity(tableName = "product_table")
data class ProductRoom(

    @PrimaryKey(autoGenerate = true)
    var pids: Long = 0,

    @ColumnInfo(name = "description")
    val description: String,
    @ColumnInfo(name = "href")
    val href: String,

    @ColumnInfo(name = "id")
    val id: Long,
    @ColumnInfo(name = "image")
    val image: String,
    @TypeConverters(TypeConverter::class)
    @ColumnInfo(name = "images")
    val images: List<Any> = emptyList(),
    @ColumnInfo(name = "name")
    val name: String,
    @TypeConverters(TypeConverter::class)
    @ColumnInfo(name = "options")
    val options: List<Any> = emptyList(),
    @ColumnInfo(name = "price")
    val price: String,
    @ColumnInfo(name = "product_id")
    val product_id: String,
    @ColumnInfo(name = "quantity")
    val quantity: Int,
    @ColumnInfo(name = "sku")
    val sku: String,
    @ColumnInfo(name = "special")
    val special: String,
    @ColumnInfo(name = "thumb")
    val thumb: String,
    @ColumnInfo(name = "zoom_thumb")
    val zoom_thumb: String


)
