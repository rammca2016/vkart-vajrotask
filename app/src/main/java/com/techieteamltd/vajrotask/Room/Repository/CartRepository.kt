package com.techieteamltd.vajrotask.Room.Repository

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.techieteamltd.vajrotask.Room.Dao.CartDao
import com.techieteamltd.vajrotask.Room.Entity.CartRoom

class CartRepository(private val cartDao: CartDao) {

    val allCartList: LiveData<List<CartRoom>> = cartDao.getAlphabetizedCart()
    val validcartList: LiveData<List<CartRoom>> = cartDao.getCartList()


    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(product: CartRoom) {
        cartDao.insert(product)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insertCart(cartList: CartRoom) {

        val id = cartList.pid?.let { cartDao.getItemId(it) }
        val qty = cartList.pid?.let { cartDao.getItemQty(it) }

        if(id == null) {

            var cartRoom = CartRoom("${cartList.pid}","${cartList.price}",1)

            cartDao.insert(cartRoom)

        }
        else {

            if(cartList.qty!!.equals(0.toLong()) ){

                if(qty?.let { it > 0.toLong() }!!) {

                    val currency = cartList.price.replace("[^0-9]".toRegex(), "")

                    var total = currency.toLong()*qty!!.minus(1)

                    var cartRoomUpdate = CartRoom("${cartList.pid}", "${total.toString()}", qty!!.minus(1))

                    cartDao.update(cartRoomUpdate)

                }else{

                    cartDao.deleteByPid(cartList.pid)

                }
            }else {

                val currency = cartList.price.replace("[^0-9]".toRegex(), "")

                var total = currency.toLong()*qty!!.plus(1)

                var cartRoomUpdate = CartRoom("${cartList.pid}", "${total.toString()}", qty!!.plus(1))

                cartDao.update(cartRoomUpdate)
            }

        }
    }
}
