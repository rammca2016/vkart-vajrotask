package com.techieteamltd.vajrotask.Room.Dao

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.techieteamltd.vajrotask.Room.Entity.CartRoom
import com.techieteamltd.vajrotask.Room.Entity.ProductRoom


@Dao
interface  ProductDao {
    @Query("SELECT * from product_table ORDER BY id ASC")
    fun getAlphabetizedProduct(): LiveData<List<ProductRoom>>

    @Query("SELECT * from product_table WHERE product_id IN (:cartRoomId) ORDER BY id ASC")
    fun getCartProduct(cartRoomId : List<String>): LiveData<List<ProductRoom>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(product: ProductRoom)

    @Query("DELETE FROM product_table")
    fun deleteAll()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllProduct(productList : List<ProductRoom>);
}