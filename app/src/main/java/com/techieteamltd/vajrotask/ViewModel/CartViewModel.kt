package com.techieteamltd.vajrotask.ViewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.techieteamltd.vajrotask.Room.Entity.CartRoom
import com.techieteamltd.vajrotask.Room.ProductRoomDatabase
import com.techieteamltd.vajrotask.Room.Repository.CartRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CartViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: CartRepository

    val allCartList: LiveData<List<CartRoom>>
    val validcartList: LiveData<List<CartRoom>>

    init {
        val cartDao = ProductRoomDatabase.getDatabase(application, viewModelScope).cartDao()
        repository = CartRepository(cartDao)
        allCartList = repository.allCartList
        validcartList = repository.validcartList
    }


    fun insert(cartRoom: CartRoom) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(cartRoom)
    }

    fun insertAll(cartRoom: CartRoom) = viewModelScope.launch(Dispatchers.IO) {
        repository.insertCart(cartRoom)
    }
}