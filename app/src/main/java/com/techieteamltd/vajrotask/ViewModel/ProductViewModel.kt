package com.techieteamltd.vajrotask.ViewModel

import android.app.Application
import androidx.lifecycle.*

import com.techieteamltd.vajrotask.Room.Entity.ProductRoom
import com.techieteamltd.vajrotask.Room.ProductRoomDatabase
import com.techieteamltd.vajrotask.Room.Repository.ProductRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ProductViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: ProductRepository

    val allProducts: LiveData<List<ProductRoom>>

    var allCartList  : MutableLiveData<List<ProductRoom>> = MutableLiveData<List<ProductRoom>>()

    val mutableProductCart: MutableLiveData<List<String>> = MutableLiveData()

    fun searchByCartId(param: List<String>) {
        mutableProductCart.value = param
    }

    init {

        val productDao = ProductRoomDatabase.getDatabase(application, viewModelScope).productDao()
        repository = ProductRepository(productDao)
        allProducts = repository.allProductList

    }


    fun insert(product: ProductRoom) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(product)
    }

    val cartProductObservable: LiveData<List<ProductRoom>> = Transformations.switchMap(mutableProductCart) { param->

        repository.getValidCartProduct(param)
    }

    fun insertAll(productList: List<ProductRoom>) = viewModelScope.launch(Dispatchers.IO) {
        repository.insertAll(productList)
    }


}