package com.techieteamltd.vajrotask.Adapters


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.techieteamltd.vajrotask.R
import com.techieteamltd.vajrotask.Room.Entity.CartRoom
import com.techieteamltd.vajrotask.Room.Entity.ProductRoom
import com.techieteamltd.vajrotask.ViewModel.CartViewModel
import java.text.NumberFormat
import java.util.*


class ProductListAdapter internal constructor(
    context: Context
) : RecyclerView.Adapter<ProductListAdapter.ProductViewHolder>() {

    private lateinit var cartViewModel: CartViewModel

    private val inflater: LayoutInflater = LayoutInflater.from(context)

    private var products = emptyList<ProductRoom>() //

    lateinit var cont : Context

    inner class ProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val productName: TextView = itemView.findViewById(R.id.product_name)

        val productImage: ImageView = itemView.findViewById(R.id.product_image)

        val productPrice: TextView = itemView.findViewById(R.id.product_price)

        val Totalcount: TextView = itemView.findViewById(R.id.Totalcount)

        val itemcount: TextView = itemView.findViewById(R.id.itemcount)

        val removeBtn: ImageButton = itemView.findViewById(R.id.removeBtn)

        val addBtn: ImageButton = itemView.findViewById(R.id.addBtn)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {

        val itemView = inflater.inflate(R.layout.product_listview, parent, false)

        cont = parent.context

        cartViewModel = ViewModelProvider(cont as AppCompatActivity).get(CartViewModel::class.java)

        return ProductViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {

        val current = products[position]


        holder.productName.text = current.name

        holder.productPrice.text = current.price

        holder.removeBtn.setOnClickListener(View.OnClickListener {

            val currency = current.price.replace("[^0-9]".toRegex(), "")

            var cartRoom = CartRoom("${current.product_id}","${currency}",0)

            cartViewModel.insertAll(cartRoom)

        })

        holder.addBtn.setOnClickListener(View.OnClickListener {

            val currency = current.price.replace("[^0-9]".toRegex(), "")

            var cartRoom = CartRoom("${current.product_id}","${currency}",1)

            cartViewModel.insertAll(cartRoom)

        })

        Glide.with(cont).load(current.image).into(holder.productImage)

        cartViewModel.allCartList.observe((cont as AppCompatActivity), Observer { products ->

            products?.let {

                for (item in products) {

                    if(current.product_id.equals(item.pid)){

                        holder.itemcount.text = item.qty.toString()

                        val numberOnly = current.price.replace("[^0-9]".toRegex(), "")

                        var totalamount : Long = numberOnly.toInt()* item.qty!!

                        val indiaLocale = Locale("en", "IN")

                        val format: NumberFormat = NumberFormat.getCurrencyInstance(indiaLocale)

                        val currency: String = format.format(totalamount)

                        if(totalamount.equals(0.toLong())) {

                            holder.Totalcount.text = null

                        }else{

                            holder.Totalcount.text = currency.toString()
                        }

                    }
                }
            }
        })
    }

    internal fun setProduct(product: List<ProductRoom>) {
        this.products = product
        notifyDataSetChanged()
    }

    override fun getItemCount() = products.size
}
